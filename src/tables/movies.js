const movieData = require("../database.json");
const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  password: "postgres",
  host: "localhost",
  port: 5432,
  database: "moviesdatabase",
});

const movies = async (movieData) => {
  await pool.query(
    `create table if not exists  movies (id serial primary key, movie_name varchar(100) unique)`
  );
  let i = 1;
  const x = movieData.reduce((acc, res) => {
    if (acc[res.Title] == undefined) {
      acc[res.Title] = i;
      i++;
      pool
        .query(`insert into movies (movie_name) values ($1)`, [res.Title])

        .catch((err) => err);
    }
    return acc;
  }, {});
  console.log(x);
  console.log("table movies created");
};

function deleteTable() {
  pool.query(`DROP TABLE IF EXISTS movies`);
  console.log("movies table deleted");
}

movies(movieData);
// deleteTable();
