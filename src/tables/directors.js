const movieData = require("../database.json");
const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  password: "postgres",
  host: "localhost",
  port: 5432,
  database: "moviesdatabase",
});

const directors = async (movieData) => {
  await pool.query(
    `create table if not exists  directors (id serial primary key, director_name varchar(40) unique)`
  );
  let i = 1;
  const x = movieData.reduce((acc, res) => {
    if (acc[res.Director] === undefined) {
      acc[res.Director] = i;
      i++;
      pool
        .query(`insert into directors (director_name) values ($1)`, [
          res.Director,
        ])

        .catch((err) => err);
    }
    return acc;
  }, {});
  console.log(x);
  console.log("table directors created");
};

function deleteTable() {
  pool.query(`DROP TABLE IF EXISTS directors`);
  console.log("directors table deleted");
}

directors(movieData);
// deleteTable();
