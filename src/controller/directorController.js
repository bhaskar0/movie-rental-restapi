const { Pool } = require("pg");
// const { request, response } = require("express");
const pool = new Pool({
  user: "postgres",
  password: "postgres",
  host: "localhost",
  port: 5432,
  database: "moviesdatabase",
});

const getAllDirectors = (request, response) => {
  pool.query("SELECT * FROM  directors", (error, result) => {
    if (error) {
      throw error;
    }
    response.status(200).json(result.rows);
  });
};

const addNewDirector = (request, response) => {
  const directorNameInput = request.body.name;

  pool.query(
    "INSERT INTO directors (director_name) VALUES ($1) RETURNING id",
    [directorNameInput],
    (error, result) => {
      if (error) {
        throw error;
      }
      response.status(201).send(`director added with ID: ${result.rows[0].id}`);
    }
  );
};
const getDirectorName = (request, response) => {
  const id = parseInt(request.params.id);
  pool.query(`SELECT * FROM directors where id=$1`, [id], (error, result) => {
    if (error) {
      throw error;
    }
    response.status(200).json(result.rows);
  });
};

const updateDirectorName = (request, response) => {
  const id = parseInt(request.params.id);
  const updatedName = request.body.name;
  pool.query(
    `UPDATE directors SET director_name=$1 WHERE id=$2`,
    [updatedName, id],
    (error) => {
      if (error) {
        throw error;
      }
      response.status(200).send(`Director details updated with ID:${id}`);
    }
  );
};

const deleteDirector = (request, response) => {
  const id = parseInt(request.params.id);
  pool.query("DELETE FROM directors where id=$1", [id], (error) => {
    if (error) {
      throw error;
    }
    response.status(200).send(`Director deleted with ID:${id}`);
  });
};
module.exports = {
  getAllDirectors,
  addNewDirector,
  getDirectorName,
  updateDirectorName,
  deleteDirector,
};
