const { Pool } = require("pg");
const { request, response } = require("express");
// const { request, response } = require("express");
const pool = new Pool({
  user: "postgres",
  password: "postgres",
  host: "localhost",
  port: 5432,
  database: "moviesdatabase",
});

const getAllMovies = (request, response) => {
  pool.query(`SELECT * FROM MOVIES`, (error, result) => {
    if (error) {
      throw error;
    }
    response.status(200).json(result.rows);
  });
};
const addNewMovie = (request, response) => {
  const newMovieName = request.body.name;

  pool.query(
    "INSERT INTO movies (movie_name) VALUES ($1) RETURNING id",
    [newMovieName],
    (error, result) => {
      if (error) {
        throw error;
      }
      response.status(201).send(`Movie added with ID: ${result.rows[0].id}`);
    }
  );
};
const getMovieName = (request, response) => {
  const id = parseInt(request.params.id);
  pool.query(`SELECT * FROM movies where id=$1`, [id], (error, result) => {
    if (error) {
      throw error;
    }
    response.status(200).json(result.rows);
  });
};

const updateMovieName = (request, response) => {
  const id = parseInt(request.params.id);
  const updatedName = request.body.name;
  pool.query(
    `UPDATE movies SET movie_name=$1 WHERE id=$2`,
    [updatedName, id],
    (error) => {
      if (error) {
        throw error;
      }
      response.status(200).send(`Director details updated with ID:${id}`);
    }
  );
};

const deleteMovie = (request, response) => {
  const id = parseInt(request.params.id);
  pool.query("DELETE FROM movies where id=$1", [id], (error) => {
    if (error) {
      throw error;
    }
    response.status(200).send(`Director deleted with ID:${id}`);
  });
};

module.exports = {
  getAllMovies,
  addNewMovie,
  getMovieName,
  updateMovieName,
  deleteMovie,
};
