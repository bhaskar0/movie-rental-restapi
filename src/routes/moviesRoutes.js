const express = require("express");
const routerMovies = express.Router();
const moviesQueries = require("../controller/moviesController");

// router.post("/", directorQueries.addNewDirector);
// router.get("/:id", directorQueries.getDirectorName);
// router.put("/:id", directorQueries.updateDirectorName);
// router.delete("/:id", directorQueries.deleteDirector);

routerMovies.get("/", moviesQueries.getAllMovies);
routerMovies.post("/", moviesQueries.addNewMovie);
routerMovies.get("/:id", moviesQueries.getMovieName);
routerMovies.put("/:id", moviesQueries.updateMovieName);
routerMovies.delete("/:id", moviesQueries.deleteMovie);

module.exports = routerMovies;
