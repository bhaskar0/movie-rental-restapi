const express = require("express");
const router = express.Router();
const directorQueries = require("../controller/directorController");

router.get("/", directorQueries.getAllDirectors);
router.post("/", directorQueries.addNewDirector);
router.get("/:id", directorQueries.getDirectorName);
router.put("/:id", directorQueries.updateDirectorName);
router.delete("/:id", directorQueries.deleteDirector);

module.exports = router;
