const express = require("express");
const app = express();
const port = 3000;
const bodyParser = require("body-parser");
const directorRoutes = require("./src/routes/directorRoutes");
const movieRoutes = require("./src/routes/moviesRoutes");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/api/directors", directorRoutes);
app.use("/api/movies", movieRoutes);
app.get("/", (req, res) => res.sendFile(__dirname + "/index.html"));
app.listen(port, () => console.log(`Example app listening on port port!`));
